<?php
	/* Partner loop template */
	$partner_image_object = get_field('partner_image');
	$partner_image_url = $partner_image_object['sizes']['medium'];
	$partner_image_alttext = $partner_image_object['alt'];
	$partner_name = get_the_title();
	$partner_url = get_field('partner_url');
	$partner_tags = get_the_tags();
	$partner_show_logo = false;
	if (has_tag('show_logo')){
		$partner_show_logo = true;
	}


?>

<div class="partner<?php if ($partner_image_url){?> partner-has-image<?php } ?> <?php if ( $partner_tags ) {foreach( $post_tags as $tag ) {echo 'tag-' . $tag->name . '  '; }} ?>">
	<?php if ($partner_url): ?>
	<a class="area-link" href="<?php echo $partner_url; ?>">
	<?php endif; ?>
	<?php if ($partner_image_url): ?>
		<div class="partner-image">
			<img src="<?php echo $partner_image_url; ?>" alt="">
		</div>
	<?php endif; ?>
		<div class="partner-text">
			<span class="partner-name"><?php echo $partner_name; ?></span>
		</div>
	<?php if ($partner_url): ?>
	</a>
	<?php endif; ?>
</div>
<?php ?>
