<?php

if ( function_exists( 'lazyblocks' ) && function_exists('get_field') ) :

	$block_template = '<?php
//Variables
$category = $attributes[\'category\'];
$show_logos = $attributes[ \'show-logos\' ];

$args = array(
	\'post_type\' => \'partner\',
	\'posts_per_page\' => -1,
);
if ($category){
	$args[\'category_name\'] = $category;
}

// The Query
$post_block_query = new WP_Query($args);

// The Loop
if ($post_block_query->have_posts()):
while ($post_block_query->have_posts()):
	$post_block_query->the_post();

	/* Partner loop template */
	$partner_image_object = get_field(\'partner_image\');
	$partner_image_url = $partner_image_object[\'sizes\'][\'medium\'];
	$partner_name = get_the_title();
	$partner_url = get_field(\'partner_url\');
	$partner_tags = get_the_tags();

?>

<div class="partner<?php if ($partner_image_url && $show_logos){?> partner-has-image<?php } ?> <?php if ( $partner_tags ) {foreach( $post_tags as $tag ) {echo \'tag-\' . $tag->name . \'  \'; }} ?>">
<?php if ($partner_url): ?>
<a class="area-link" href="<?php echo $partner_url; ?>">
<?php endif; ?>
<?php if ($partner_image_url && $show_logos): ?>
	<div class="partner-image">
		<img src="<?php echo $partner_image_url; ?>" alt="">
	</div>
<?php endif; ?>
	<div class="partner-text">
		<span class="partner-name"><?php echo $partner_name; ?></span>
	</div>
<?php if ($partner_url): ?>
</a>
<?php endif; ?>
</div>

<?php
endwhile;
endif;
wp_reset_postdata();
?>';

lazyblocks()->add_block( array(
	'id' => 80,
	'title' => 'Partners List',
	'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z" /></svg>',
	'keywords' => array('logo','partners'),
	'slug' => 'lazyblock/partners-list',
	'description' => 'Show a large list of partners. (This block is added by the Baseline – Partners plugin)',
	'category' => 'lazyblocks',
	'category_label' => 'lazyblocks',
	'supports' => array(
		'customClassName' => true,
		'anchor' => true,
		'align' => array(
			0 => 'wide',
			1 => 'full',
		),
		'html' => false,
		'multiple' => true,
		'inserter' => true,
  ),
  'ghostkit' => array(
    'supports' => array(
      'spacings' => false,
      'display' => false,
      'scrollReveal' => false,
      'frame' => false,
      'customCSS' => false,
    ),
  ),
  'controls' => array(
    'control_6f5ab942ff' => array(
      'type' => 'text',
      'name' => 'category',
      'default' => '',
      'label' => 'Filter by category',
      'help' => 'Optional. Use the all-lowercase, no spaces \'slug\' version of the category name. (ex: "my-category-name", etc)',
      'child_of' => '',
      'placement' => 'inspector',
      'width' => '100',
      'hide_if_not_selected' => 'false',
      'save_in_meta' => 'false',
      'save_in_meta_name' => '',
      'required' => 'false',
      'placeholder' => '',
      'characters_limit' => '',
    ),
		'control_393bed4a15' => array(
			'type' => 'toggle',
			'name' => 'show-logos',
			'default' => '',
			'label' => 'Show logos',
			'help' => 'Turn on to show logos (if uploaded), turn off for a text-only list.',
			'child_of' => '',
			'placement' => 'inspector',
			'width' => '100',
			'hide_if_not_selected' => 'false',
			'save_in_meta' => 'false',
			'save_in_meta_name' => '',
			'required' => 'false',
			'checked' => 'true',
			'alongside_text' => '',
			'placeholder' => '',
			'characters_limit' => '',
		),
  ),
  'code' => array(
    'output_method' => 'php',
    'editor_html' => '',
    'editor_callback' => '',
    'editor_css' => '',
    'frontend_html' => $block_template,
    'frontend_callback' => '',
    'frontend_css' => '',
    'show_preview' => 'always',
    'single_output' => true,
  ),
  'condition' => array(),
) );

else:
	function admin_notice__error() {
	    $class = 'notice notice-error';
	    $message = __( 'The Baseline – Partners requires the Advanced Custom Fields and Lazy Blocks plugins to be active.', 'bl-partners' );

	    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	}
	add_action( 'admin_notices', 'admin_notice__error' );

endif;

?>
