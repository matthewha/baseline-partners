<?php
/*
Plugin Name: Baseline — Partners
Plugin URI:
Description: Add lists of partners to a Baseline website, either as a text list or a logo wall.
Version: 1.0
Author: Matthew Hinders-Anderson
Author URI: http://wehtt.am
*/

/* NOTE: *************


// Define urls/paths that will be used throughout the plugin
define( 'blpartner_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'blpartner_PLUGIN_CLASS_DIR', blpartner_PLUGIN_DIR.'classes/' );

*/
class blpartner_Class {

	public function __construct() {
		add_action( 'init', array( $this, 'add_baseline_partner_type' ) );
		add_action( 'plugins_loaded', [$this, 'plugins_loaded' ], 50);
	}

	public function enqueue_baseline_partner_style(){
		global $post;
		if ($post->post_type == "partner"){
			$blpartner_plugin_directory = plugin_dir_url( __FILE__ );
		}
	}

	/**
	* Register the custom post type.
	* @link http://codex.wordpress.org/Function_Reference/register_post_type
	*/
	public function add_baseline_partner_type() {
		$labels = array(
			'name'               => _x( 'Partners', 'post type general name', 'blpartner' ),
			'singular_name'      => _x( 'Partner', 'post type singular name', 'blpartner' ),
			'menu_name'          => _x( 'Partners', 'admin menu', 'blpartner' ),
			'name_admin_bar'     => _x( 'Partner', 'add new on admin bar', 'blpartner' ),
			'add_new'            => _x( 'Add New', 'Press Release', 'blpartner' ),
			'add_new_item'       => __( 'Add New Partner', 'blpartner' ),
			'new_item'           => __( 'New Partner', 'blpartner' ),
			'edit_item'          => __( 'Edit Partner', 'blpartner' ),
			'view_item'          => __( 'View Partner', 'blpartner' ),
			'all_items'          => __( 'All Partners', 'blpartner' ),
			'search_items'       => __( 'Search Partners', 'blpartner' ),
			'parent_item_colon'  => __( 'Parent Partners:', 'blpartner' ),
			'not_found'          => __( 'No Partners found.', 'blpartner' ),
			'not_found_in_trash' => __( 'No Partners found in Trash.', 'blpartner' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'query_var'          => true,
			'rewrite' 			 => array( 'slug' => __('partner','blpartner'), 'with_front' => true),
			'capability_type'    => 'page',
			'has_archive'        => false,
			'hierarchical'       => false,
			'can_export' 	     => true,
			'taxonomies'		=> array('post_tag'),
			'menu_icon'			 => 'dashicons-groups',
			'menu_position'      => null,
			'supports'			 => array('title', 'custom-fields', 'revisions'),
		);

		register_post_type( 'partner', $args );


	}
	// Flush the rewrite rules to prevent 404s when the plugin is first turned on.
	public function baseline_partner_activate() {
	    flush_rewrite_rules();
	}
	// Load the Partner list block template
	public function plugins_loaded(){
		include_once __DIR__ . '/classes/block-partner.php';
	}

}
$blpartner = new blpartner_Class();

register_activation_hook( __FILE__, array( 'blpartner_Class', 'baseline_partner_activate' ) );
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );


function add_partner_stylesheet(){
	wp_register_style('bl-partner-css', '/wp-content/plugins/baseline-partners/css/baseline-partner-style.css');
	wp_enqueue_style('bl-partner-css');
}
add_action( 'wp_print_styles', 'add_partner_stylesheet' );

/*
* Advanced Custom Fields for Baseline Partner Plugin
@ Depends on ACF WP Plugin
*
*/
// Add ACF custom fields for Posters
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5fa58b2d9bbff',
	'title' => 'Partner',
	'fields' => array(
		array(
			'key' => 'field_5fa58b99d07fa',
			'label' => 'Partner logo',
			'name' => 'partner_image',
			'type' => 'image',
			'instructions' => 'A web-friendly image file of the logo (.jpg, .png, .gif)',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5fa58bd3d07fc',
			'label' => 'Partner URL',
			'name' => 'partner_url',
			'type' => 'url',
			'instructions' => 'URL of the partner website. Optional.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'partner',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;

?>
