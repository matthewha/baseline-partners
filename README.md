# Baseline - Partners

For adding and managing large groups of partnering organizations. Adds a 'partner' custom post type as well as a "partners" block for the block editor. A plugin for the Baseline Wordpress theme framework.